FROM debian:buster

#Update du system
RUN apt-get update \
&& apt-get upgrade -y

#Installation des prérequis
RUN apt-get install -y python3-yaml \
git curl wget \
libnewt-dev libssl-dev libncurses5-dev \
subversion libsqlite3-dev build-essential \
libjansson-dev libxml2-dev uuid-dev sed apt-utils

#Copie des fichiers Asterisk
RUN cd /root \
&& curl -O http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-16-current.tar.gz \
&& tar -xzf asterisk-*.tar.gz \
&& rm asterisk-*.tar.gz -f

#Installation des prérequis
RUN cd /root/asterisk-* \
&& export DEBIAN_FRONTEND=noninteractive \
&& contrib/scripts/install_prereq install \
&& ln -s /bin/sed /usr/bin/sed \
&& contrib/scripts/get_mp3_source.sh

#Installation de Asterisk
RUN cd /root/asterisk-* \
&& ln -s /bin/grep /usr/bin/grep \
&& ./configure \
&& make menuselect/menuselect menuselect-tree menuselect.makeopts \
&& menuselect/menuselect \
--enable chan_ooh323 --enable format_mp3 \
--enable CORE-SOUNDS-EN-WAV --enable CORE-SOUNDS-EN-ULAW --enable CORE-SOUNDS-EN-ALAW --enable CORE-SOUNDS-EN-GSM --enable CORE-SOUNDS-EN-G729 --enable CORE-SOUNDS-EN-G722 --enable CORE-SOUNDS-EN-SLN16 --enable CORE-SOUNDS-EN-SIREN7 --enable CORE-SOUNDS-EN-SIREN14 \
--enable CORE-SOUNDS-FR-WAV --enable CORE-SOUNDS-FR-ULAW --enable CORE-SOUNDS-FR-ALAW --enable CORE-SOUNDS-FR-GSM --enable CORE-SOUNDS-FR-G729 --enable CORE-SOUNDS-FR-G722 --enable CORE-SOUNDS-FR-SLN16 --enable CORE-SOUNDS-FR-SIREN7 --enable CORE-SOUNDS-FR-SIREN14 \
--enable MOH-OPSOUND-WAV --enable MOH-OPSOUND-ULAW --enable MOH-OPSOUND-WAV --enable MOH-OPSOUND-ALAW --enable MOH-OPSOUND-GSM --enable MOH-OPSOUND-G729 --enable MOH-OPSOUND-G722 --enable MOH-OPSOUND-SLN16 --enable MOH-OPSOUND-SIREN7 --enable MOH-OPSOUND-SIREN14 \
--enable EXTRA-SOUNDS-EN-WAV --enable EXTRA-SOUNDS-EN-ULAW --enable EXTRA-SOUNDS-EN-ALAW --enable EXTRA-SOUNDS-EN-GSM --enable EXTRA-SOUNDS-EN-G729 --enable EXTRA-SOUNDS-EN-G722 --enable EXTRA-SOUNDS-EN-SLN16 --enable EXTRA-SOUNDS-EN-SIREN7 --enable EXTRA-SOUNDS-EN-SIREN14 \
--enable EXTRA-SOUNDS-FR-WAV --enable EXTRA-SOUNDS-FR-ULAW --enable EXTRA-SOUNDS-FR-ALAW --enable EXTRA-SOUNDS-FR-GSM --enable EXTRA-SOUNDS-FR-G729 --enable EXTRA-SOUNDS-FR-G722 --enable EXTRA-SOUNDS-FR-SLN16 --enable EXTRA-SOUNDS-FR-SIREN7 --enable EXTRA-SOUNDS-FR-SIREN14 \
menuselect.makeopts \
&& make \
&& make install \
&& make samples \
&& make config \
&& ldconfig

#Installation Soft-Modem
RUN cd /root/asterisk-* \
&& git clone https://github.com/johnnewcombe/asterisk-Softmodem.git \
&& cp asterisk-Softmodem/app_softmodem.c apps/. \
&& make apps  \
&& cp apps/app_softmodem.so /usr/lib/asterisk/modules/.

#Copie des configurations
COPY asterisk /etc/asterisk

#Création de l'utilisateur
RUN groupadd asterisk \
&& useradd -r -d /var/lib/asterisk -g asterisk asterisk \
&& usermod -aG audio,dialout asterisk \
&& chown -R asterisk:asterisk /etc/asterisk \
&& chown -R asterisk:asterisk /var/log/asterisk \
&& chown -R asterisk:asterisk /var/spool/asterisk \
&& chown -R asterisk:asterisk /usr/lib/asterisk

#Redémarrage des services
RUN service asterisk restart

#Installation du serveur Minitel
RUN cd /root/ && git clone https://github.com/BwanaFr/minitel-server.git
RUN apt install python3-pip -y \
&& pip3 install -U PyYAML
COPY minitel-server /root/minitel-server

#Lancement du serveur Minitel
RUN apt install screen -y
CMD service asterisk start \
&& cd /root/minitel-server \
&& screen -d -m python3 MinitelSrv.py \
&& bash
