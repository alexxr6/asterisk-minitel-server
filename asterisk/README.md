This folders contains all Asterisk configurations files (needed)

-extensions.conf contains the "sips" context used by the PJSIP user

-pjsip.conf contains the SIP users (6000 to 6004), all password have been set to 1234 and each user can be connected on 10 differents IP addresses.

-rtp.conf contains the RTP (Real-time Transport Protocol) range (set from 10000 to 10100)
