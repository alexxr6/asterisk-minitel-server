This folder contains the minitel server configuration.

This minitel server is maintained on this GIT Repository : 

[https://github.com/BwanaFr/minitel-server](https://github.com/BwanaFr/minitel-server)

It create a minitel server for each folders on the configuration.yaml pages_folder path.

I used his plugin for the project, all the credit for this part goes to [BwanaFR](https://github.com/BwanaFr).