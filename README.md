# Asterisk-Minitel-Server

Minitel server based on Asterisk 16 on Debian 10

This docker container can be deployed to provide multiples Minitel servers (100 from 3600 to 3699)

This project use the official Asterisk source and compile it with the menuselect options

This project also include the Asterisk plugin asterisk-Softmodem. This plugin is maintained for recent Asterisk versions via this [Fork](https://github.com/johnnewcombe/asterisk-Softmodem.git) of the [initial project](https://github.com/proquar/asterisk-Softmodem)

The minitel server used for this project is maintained [here](https://github.com/BwanaFr/minitel-server).

## Requirement
This docker container need docker.
You can check the installation process for your operating system on this [page](https://docs.docker.com/get-docker/)

## Creating the container
```
git clone https://gitlab.com/alexxr6/asterisk-minitel-server.git
cd asterisk-minitel-server
docker build asterisk-minitel-server .
```

## Starting the container and keep it attached
Do not forget to change the ports with the one configured on the asterisk/rtp.conf
```
docker run -network host --tty --interactive asterisk
```

## Starting the container
Do not forget to change the ports with the one configured on the asterisk/rtp.conf
```
docker run -network host
```

## Upcoming modifications
I may be able to split the Docker container in multiples containers (Asterisk with Softmodem and the minitel server) so I can create a docker-compose file.

## Credits
All credits for the Asterisk part of this project goes to [Asterisk](https://www.asterisk.org/)

All credits for the Asterisk Softmodem plugin goes to [johnnewcombe](https://github.com/johnnewcombe) for the Asterisk 16 compatible fork, and to [proquar](https://github.com/proquar) for the initial project

All credits for the Minitel Server goes to [BwanaFR](https://github.com/BwanaFr)